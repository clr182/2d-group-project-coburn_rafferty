class Enemy {
    constructor(img, X, Y, Width, Height, Health){
        this.Width = Width;
        this.Height= Height;
        this.X     = X;
        this.Y     = Y;
        //this.Behaviors = Behaviors || [];
        this.Health = Health;
        this.img = img;
        this.X2 = this.getX2();
        this.Y2 = this.getY2();
    };

    setWidth (Width){ return this.Width = Width; };
    getWidth (){ return this.Width; };
 
    setHeight (Height){return this.Height = Height;};
    getHeight (){ return this.Height; };
 
    setX1 (X){ return this.X = X; };
    getX1 (){ return this.X; };


    getX2 (){ return this.X + this.Width; };
 
    setY1 (Y){ return this.Y = Y; };
    getY1 (){ return this.Y; };

    getY2 (){ return this.Y+ this.Height };
 
    setBehaviors (Behaviors){ return this.Behaviors = Behaviors; };
    getBehaviors (){ return this.Behaviors; };
 
    setHealth (Health){ return this.Health = Health; };
    getHealth (){ return this.Health; };
 
    setImg (img){ return this.img = img; };
    getImg (){ return this.img; };

    calcArea (){
        return (this.Height * this.Width);
    };
 
    calcObjectMidpoint(){
        var point1 = (this.X + this.X2)/2;
        var point2 = (this.Y + this.Y2)/2;
        var points = [point1, point2];
        return points;
    };

    subtractHitPoints (){
        this.Health--;
    };

    showHealthBar() {
        if(this.Health == 3){
            game.context.fillStyle = "green";
        }
        else if(this.Health == 2){
            game.context.fillStyle = "orange";
        }
        else if(this.Health == 1){
            game.context.fillStyle = "red";
        }
        else if(this.Health === 0){
            this.removeenemy();
        }
        
        game.context.fillRect(this.X, this.Y - 10, this.Width, 5);
        //game.context.fillText(this.Health, this.X + 20, this.Y -5);
        game.context.fill();
    };

    removeenemy(){
        this.hide();
    };
}

