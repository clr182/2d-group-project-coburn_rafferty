# CA2 GROUP ASSIGNMENT

## A grotesque First Person Space Shooter with point and click controls

## Developed by Christian Rafferty

## References
I pulled a lot of my images from the works of H.R. Giger.
Audio written by Christian Rafferty.


##I had some of my friends and family test my game and here is their response
link: ``https://forms.gle/rNeuswcU2AWkpzFWA``
![user feedback](form1.PNG)
**Figure 1**

![user feedback](form2.PNG)
**Figure 2**

![user feedback](form3.PNG)
**Figure 3**

As you may notice in figure 3, at the time of review the game was incomplete and buggy.

##Performance
I tested performance of the game running and playing and found that when playing the game if the user
clicks a lot in rapid succession, frames will drop dramatically, from a solid 60, to below 30.
_evidant in figure 4_
This is caused because when the user clicks the mouse, the canvas is cleared and redrawn, so the laser beams would not linger on screen.

![performance](Performance.PNG)
**Figure 4**

##Hosted on 000webhost
``https://2dassignment.000webhostapp.com/game.html?fbclid=IwAR3zqBK9VYbH14xVz7Lrw4OIa_aZDuII6Bx6meidQsZW3dec1RRIoTmTasE``

Thanks for playing,
Christian Rafferty.