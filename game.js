var Game = function(){
    this.canvas = document.getElementById('game-canvas'),
    this.context = this.canvas.getContext('2d'),
    this.fpsElement = document.getElementById('fps'),


//key input
    this.LEFT_ARROW = 37,
    this.RIGHT_ARROW = 39,
    this.UP_ARROW = 38,
    this.DOWN_ARROW = 40,
    this.P_key = 80,

//Time
    this.timeSystem = new TimeSystem(),
    this.timeRate = 1,

//Gravity
    this.GRAVITY_FORCE = 9.81, //m/s/s

//FPS & animation
    this.FPS = 30,
    this.animate = null,

//States
    this.running = true;
    this.paused  = false;

//mouse input
    this.mouseX = 0,
    this.mouseY = 0,
    this.newCoords = [],
    this.oldCoords = [],

    this.EMPTY  = 0,
    this.PLAYER = 1,
    this.ENEMY  = 2,
    this.ITEM   = 3,
    this.PLAYERSTART = 10,
    
    this.SCORE = 0,

    this.enemyX = 30,
    this.enemyY = 30,
    this.enemyW = 70,
    this.enemyH = 30,

    this.laserBeams;

//walls

    this.playerPosition;

    this.leftWallX  = 250,
    this.leftWallY1 = 199,
    this.leftWallY2 = 199,

    this.rightWallX  = 210,
    this.rightWallY1 = 30,
    this.rightWallY2 = 60,

    this.enemies = [],
    this.enemyHealth = 1;

//images
    this.ship       = new Image(); this.ship.src = 'Images/ship.png';
    this.enemy      = new Image(); this.enemy.src = 'Images/enemy.png';
    this.boss       = new Image(); this.boss.src = 'Images/boss.png';
    this.boss_heart = new Image(); this.boss_heart.src = 'Images/boss_heart.png';
    this.boss_lip   = new Image(); this.boss_lip.src = 'Images/boss_lip.png';
    this.wall       = new Image(); this.wall.src = 'Images/wall.jpg';
    this.walls      = new Image(); this.walls.src = 'Images/walls.png';
    this.floor      = new Image(); this.floor.src = 'Images/floor.jpg';
    this.ceiling    = new Image(); this.ceiling.src = 'Images/ceiling.jpg';


//audio
    this.theme      = new Audio('Audio/isolating_theme.wav');
    this.lasersound = new Audio('Audio/laser.wav');

    this.theme.loop = true;
    this.theme.play();
}

Game.prototype = {
    animate: function(now){
       //Replace the time that is worked out by the browser
       //with our own gametime
       now = game.timeSystem.calculateGameTime();
        if(game.paused){
            setTimeout(function () {
                requestAnimationFrame(game.animate);
            }, game.PAUSE_CHECK_INTERVAL);
        }
        else{
            game.fps = game.calculateFps(now);
            game.draw(now);
            game.lastAnimationFrameTime = now;
            requestAnimationFrame(game.animate);
        }
    },


    setTimeRate: function(rate){
       
        this.timeRate = rate;
        this.timeSystem.setTransducer(function(now){
           return now * snailBait.timeRate;
        });
     },

    calculateFps: function(now){
        var fps = 1/(now - this.lastAnimationFrameTime) * 1000 * game.timeRate;
        if(now - this.lastFpsUpdateTime > 1000){
            this.lastFpsUpdateTime = now;
            this.fpsElement.innerHTML = fps.toFixed(0) + ' fps';
        }

        if(fps >= 25){
            paused = true;
            alert("This game is running slow. \n Are you sure you want to continue playing?");
        }
        return fps;
    },

    removeIntroText: function(){
        var intro = document.getElementById("introText");
        intro.innerHTML = "";
    },

    drawShip: function(){
        images = [
            game.context.drawImage(game.ship, 0, 0, 800, 400),
        ];
    },

    HUDElements: function(){
        game.drawScreens();
        game.drawHealth();
        game.drawRadar(); 
    },

    drawHealth: function(){
        game.context.beginPath();
        game.context.moveTo(275, 400);
        game.context.lineTo(295, 380);
        game.context.lineTo(500, 380);
        game.context.lineTo(520, 400);
        game.context.strokeStyle = "orange";
        game.context.fillStyle = "#00FF00";
        game.context.lineWidth = 1;
        game.context.fill();
        game.context.stroke();
        game.context.closePath();
    },

    drawScreens: function(){
        game.context.beginPath();
        game.context.rect(333, 280, 133, 85);
        game.context.rect(263, 295, 66, 62);
        game.context.rect(480, 295, 66, 62);
        game.context.strokeStyle = "red";
        game.context.fillStyle = "green";
        game.context.lineWidth = 1;
        game.context.fill();
        game.context.stroke();
        game.context.closePath();

        game.context.fillStyle = "blue";
        game.context.fillText("Score:\n " + game.SCORE, 490, 330, 55, 51);
    },

    drawRadar: function(){
        game.context.beginPath();
        game.context.rect(345, 287, 1, 70);
        game.context.rect(358, 287, 1, 70);
        game.context.rect(371, 287, 1, 70);
        game.context.rect(384, 287, 1, 70);
        game.context.rect(397, 287, 1, 70);
        game.context.rect(410, 287, 1, 70);

        game.context.rect(338, 295, 80, 1);
        game.context.rect(338, 305, 80, 1);
        game.context.rect(338, 315, 80, 1);
        game.context.rect(338, 325, 80, 1);
        game.context.rect(338, 335, 80, 1);
        game.context.rect(338, 345, 80, 1);
        game.context.strokeStyle = "#75b76e";
        game.context.fillStyle = "white";
        game.context.lineWidth = 1;
        game.context.fill();
        game.context.stroke();
        game.context.closePath();

        game.context.beginPath();
        game.context.arc(443, 320, 15, 0, 2* Math.PI);
        game.context.arc(443, 320, 10, 0, 2* Math.PI);
        game.context.strokeStyle = "#75b76e";
        game.context.lineWidth = 1;
        game.context.stroke();
        game.context.closePath();
    },

    
    playerLasers: function(){
        
        game.context.globalCompositeOperation = "lighter";
        game.context.shadowBlue = 10;
        game.context.shadowColor = '#FD0100';
        game.context.beginPath();
        game.context.moveTo(200, 400);
        game.context.lineTo(this.mouseX, this.mouseY);
        game.context.strokeStyle = "red";
        game.context.lineWidth = 3;
        game.context.fillStyle = "red";
        game.context.fill();
        game.context.stroke();
        game.context.closePath();
        
        game.context.beginPath();
        game.context.moveTo(600, 400);
        game.context.lineTo(this.mouseX, this.mouseY);
        game.context.strokeStyle = "red";
        game.context.lineWidth = 3;
        game.context.fillStyle = "red";
        game.context.fill();
        game.context.stroke();
        game.context.closePath();
        game.context.globalCompositeOperation = "source-over";
        this.lasersound.play();
    },

    drawWalls: function(){
        game.context.drawImage(game.floor, 0, 200, 800, 200);
        game.context.drawImage(game.wall, 0, -30, 800, 150);  
        game.context.drawImage(game.boss_heart, 353, 95, 90, 85);
        game.context.drawImage(game.boss_lip, 351, 118, 95, 75);
        game.context.drawImage(game.boss, 242, 40, 315, 160);
        game.context.drawImage(game.walls, 0, 0, 800, 400);

    },

    setPlayerPosition: function(x){
            this.playerPosition = x;
            return this.playerPosition;
    },
    
    setWallChange: function(playerPosition){
        if(this.playerPosition === "LEFT"){
            this.leftWallX = 120;
        }
        else if(this.playerPosition === "CENTER"){
            this.leftWallX = 90;
        }
        else if(this.playerPosition === "RIGHT"){
            this.leftWallX = 30;
        }
    },

    drawEnemy: function(){
        let enemySpawnPoint = [
            { top: 40, left: 256, bot: 200, right: 551 },
        ];
        
        var spawnX = Math.floor((Math.random()* (enemySpawnPoint[0].right - enemySpawnPoint[0].left)) + enemySpawnPoint[0].left);
        var spawnY = Math.floor((Math.random()* (enemySpawnPoint[0].bot - enemySpawnPoint[0].top)) + enemySpawnPoint[0].top);

        console.log(spawnX + " " + spawnY);
        //game.context.drawImage(game.enemy, spawnX, spawnY, 50, 20);
        var metroid = new Enemy(game.enemy, spawnX, spawnY, 50, 20, this.enemyHealth);
        
        game.enemies[0] = metroid;
        
        game.context.drawImage(game.enemies[0].img, game.enemies[0].X, game.enemies[0].Y,
                                game.enemies[0].Width, game.enemies[0].Height);
        metroid.showHealthBar();
        metroid.detectHit(game.mouseX, game.mouseY);
        metroid.calcObjectMidpoint();
    },

    screenShake: function(){
        document.getElementById("game-canvas").style.transform = "rotate(180deg)";
    },

    draw: function(){
        requestAnimationFrame(game.drawWalls);
        requestAnimationFrame(game.drawEnemy);
        game.playerLasers();
        requestAnimationFrame(game.drawShip);
        requestAnimationFrame(game.HUDElements);
    },

    startGame: function(){
        this.playing = true;
        this.timeSystem.start();
        game.timeSystem.calculateGameTime();
        game.setTimeRate(1.0);
        requestAnimationFrame(this.animate);
    },

    togglePaused: function(){
        var now = this.timeSystem.calculateGameTime();
        this.paused = !this.paused;

        // this.togglePausedStateOfAllBehaviors(now);

        if(this.paused){
            this.pauseStartTime = now;
        }
        else{
            this.lastAnimationFrameTime += 
            (now - this.pauseStartTime);
        }
    },


    
}

var game = new Game();

window.addEventListener('keydown', function(e){
    var key = e.keyCode;
    
    if(key === game.LEFT_ARROW){
        game.setPlayerPosition("LEFT");
        game.setWallChange();
    }
    else if(key === game.RIGHT_ARROW){
        game.setPlayerPosition("CENTER");
        game.setWallChange();
        console.log("right");
    }
    else if (key === game.P_key){
        game.togglePaused();
    
    }
});

document.addEventListener("touchstart", touchHandler);

function touchHandler(e){
    if(e.touches) {
        game.mouseX = e.clientX;
        game.mouseY = e.clientY;
        game.mouseX -= game.canvas.offsetLeft;
        game.mouseY -= game.canvas.offsetTop;
        
        takeMousePositionAddToArray(game.mouseX, game.mouseY);
        game.removeIntroText();
        
        game.draw(game.drawShip);
        
    }
}

window.addEventListener('click', function (e){
    game.mouseX = e.clientX;
    game.mouseY = e.clientY;
    game.mouseX -= game.canvas.offsetLeft;
    game.mouseY -= game.canvas.offsetTop;
    
    takeMousePositionAddToArray(game.mouseX, game.mouseY);
    game.removeIntroText();
    
    game.draw(game.drawShip);
    
});

function takeMousePositionAddToArray(xCoords, yCoords){

    game.newCoords[0] = xCoords;
    game.newCoords[1] = yCoords;
}





